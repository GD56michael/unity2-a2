﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Outpost : MonoBehaviour
{
    public static List<Outpost> OutpostList = new List<Outpost>();

    [SerializeField]
    private Renderer _Renderer;

    [SerializeField]
    private float _CaptureTime = 5f;

    public float CaptureValue { get; private set; }
    public int CurrentTeam { get; private set; }

    public static Outpost GetRandomOutpost()
    {
        if (OutpostList.Count == 0) return null;

        var randomIndex = Random.Range(0, OutpostList.Count);
        return OutpostList[randomIndex];
    }

    private void OnEnable()
    {
        GetComponent<Collider>().isTrigger = true;
        OutpostList.Add(this);
    }

    private void Update()
    {
        var teamColor = GameManager.Instance.TeamColors[CurrentTeam];
        _Renderer.material.color = Color.Lerp(Color.white, teamColor, CaptureValue);
    }

    private void OnDisable()
    {
        OutpostList.Remove(this);
    }

    private void OnTriggerStay(Collider other)
    {
        var unit = other.GetComponent<Unit>();
        if (unit == null || unit.IsAlive == false)
        {
            return;
        }

        if (unit.TeamNumber == CurrentTeam)
        {
            CaptureValue += Time.fixedDeltaTime / _CaptureTime;
            if (CaptureValue > 1f) CaptureValue = 1f;
        }
        else
        {
            CaptureValue -= Time.fixedDeltaTime / _CaptureTime;
            if (CaptureValue <= 0f)
            {
                CaptureValue = 0f;
                CurrentTeam = unit.TeamNumber;
            }
        }
    }
}
