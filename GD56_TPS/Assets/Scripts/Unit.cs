﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public abstract class Unit : MonoBehaviour
{
    public bool IsAlive { get; protected set; } = true;

    [Header("Unit")]
    public int TeamNumber = 0;

    [SerializeField] private Renderer _Renderer;

    [SerializeField] private float _Health = 100f;

    [Header("Attack"), SerializeField] private Laser _LaserPrefab;

    [SerializeField] private float _AttackDamage = 8f;

    [SerializeField] protected float _invulnTime;
    protected float invulnTimeLeft;
    protected bool isInvuln = false;

    [SerializeField] protected float _colorLerpTime;
    protected Color nextColor;

    [SerializeField] protected Material _unitMat;

    protected Color teamColor;

    protected Eye[] _Eyes;

    protected Rigidbody _RB;
    protected Animator _Anim;

    protected abstract void UnitAwake();


    protected void Awake()
    {
        _RB = GetComponent<Rigidbody>();
        _Anim = GetComponent<Animator>();
        _Eyes = GetComponentsInChildren<Eye>();

        RandomColor();

        UnitAwake();
    }

    protected void Start()
    {
        SetTeam(TeamNumber);
    }

    protected virtual void Update()
    {
        InvulnState();
    }

    public void InvulnTrigger(){
        isInvuln = true;
        invulnTimeLeft = _invulnTime;
    }

    private void InvulnState()
    {
        if (isInvuln)
        {
            invulnTimeLeft -= Time.deltaTime;
            _Renderer.material.color = Color.Lerp(_Renderer.material.color, nextColor, Time.deltaTime * _colorLerpTime);

            Debug.Log(nextColor);

            if (_Renderer.material.color == nextColor)
            {
                RandomColor();
            }

            if (invulnTimeLeft < 0)
            {
                isInvuln = false;
            }
        }
        else {
            _Renderer.material.color = teamColor;
        }
    }

    private void RandomColor()
    {
        nextColor = new Color(Random.value, Random.value, Random.value);
    }

    private void SetTeam(int teamNumber)
    {
        TeamNumber = teamNumber;
        teamColor = GameManager.Instance.TeamColors[TeamNumber];

        if (_Renderer == null) return;

        _Renderer.material.color = teamColor;
    }

    protected bool CanSee(Transform otherTransform, Vector3 hitPosition)
    {
        foreach (var eye in _Eyes)
        {
            var startPos = eye.transform.position;
            var dir = hitPosition - startPos;
            var ray = new Ray(startPos, dir);
            if (Physics.Raycast(ray, out var hit))
            {
                if (hit.transform == otherTransform)
                {
                    return true;
                }
            }
        }
        return false;
    }

    protected void ShootLasersFromEyes(Transform other, Vector3 hitPos)
    {
        foreach (var eye in _Eyes)
        {
            Instantiate(_LaserPrefab).Shoot(eye.transform.position, hitPos);
        }

        var otherUnit = other.GetComponent<Unit>();
        otherUnit?.TakeDamage(_AttackDamage);
    }

    public void TakeDamage(float damage)
    {
        if(!isInvuln)
        {
            _Health -= damage;
            if (_Health <= 0f)
            {
                _Health = 0f;
                Die();
            } 
        }
    }

    virtual protected void Die()
    {
        IsAlive = false;
        _Anim.SetBool("IsAlive", false);
    }
}
