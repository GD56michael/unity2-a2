﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvulnObject : MonoBehaviour
{
    [SerializeField] private Renderer _Renderer;
    [SerializeField] private float _colorLerpTime;
    private Color nextColor;

    // Start is called before the first frame update
    void Start()
    {
        RandomColor();
    }

    // Update is called once per frame
    void Update()
    {
        _Renderer.material.color = Color.Lerp(_Renderer.material.color, nextColor, Time.deltaTime * _colorLerpTime);

        if (_Renderer.material.color == nextColor)
        {
            RandomColor();
        }
    }

    private void RandomColor()
    {
        nextColor = new Color(Random.value, Random.value, Random.value);
    }

    void OnCollisionEnter(Collision collision)
    {
        var _test = collision.gameObject;
        //if(_test.GetType == Unit){
        //    Debug.Log("beanboi");
        //}
    }
}
